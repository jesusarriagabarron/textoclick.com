// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import store from './store'

Vue.config.productionTip = false
Vue.use(Vuex);

/* eslint-disable no-new */

import VueI18n from 'vue-i18n'
import messages from './translate/spanish.js'

Vue.use(VueI18n)

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'es', // set locale
  messages, // set locale messages
})

new Vue({
  el: '#app',
  router,
  i18n,
  store,
  template: '<App></App>',
  components: { App,  },
}).$mount('#app');
