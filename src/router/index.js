import Vue from 'vue'
import Router from 'vue-router'
import Register from './../components/Register'
import Login from './../components/Login'
import Profile from './../components/Profile'
import ProfileEdit from './../components/ProfileEdit'
import ChangePassword from './../components/ChangePassword'
import ProfileMisArticulos from './../components/ProfileMisArticulos'
import Confirmation from './../components/Confirmation'
import Activated from './../components/Activated'
import SoyRedactor from './../components/SoyRedactor'
import SoyComprador from './../components/SoyComprador'
import SendArticle from './../components/SendArticle'
import store from './../store'
import ShowTests from './../components/ShowTests'
import TestStart from './../components/TestStart'
import ArticleShow from './../components/article/ArticleShow'

Vue.use(Router)


function guest(next){
  if(store.getters.isAuth){
    next('/');
  }
  else{
    next();
  }
}

function onlyValid(next){
  if(!store.getters.isAuth){
    next('/');
  }
  else{
    next();
  }
}



export default new Router({
  mode: 'history',
  routes: [
    {
      //Home route
      path: '/',
      name:'homepage'
    },
    {
      //Sign up
      path:'/sign-up',
      name:'register',
      component: Register,
      beforeEnter: (to, from, next) => {
        guest(next);
      }
    },
    {
      //Login
      path:'/login',
      name:'login',
      component: Login,
      beforeEnter: (to, from, next) => {
        guest(next);
      }
    },
    {
      //Login
      path:'/confirmation',
      name:'confirmation',
      component: Confirmation
    },
    {
      //Login
      path:'/activated',
      name:'activated',
      component: Activated
    },
    {
      //Show User Profile
      path:'/profile',
      name: 'profile',
      component: Profile,
      beforeEnter: (to, from, next) => {
        onlyValid(next);
      }
    },
    {
      //Show User Profile
      path:'/writter/tests',
      name: 'showTests',
      component: ShowTests,
      beforeEnter: (to, from, next) => {
        onlyValid(next);
      }
    },
    {
      //Edit Profile
      path:'/profile/edit',
      name: 'profileEdit',
      component: ProfileEdit,
      beforeEnter: (to, from, next) => {
        onlyValid(next);
      }
    },
    {
      //View Profile article
      path:'/profile/my-articles',
      name: 'profileMisArticulos',
      component: ProfileMisArticulos,
      beforeEnter: (to, from, next) => {
        onlyValid(next);
      }
    },
    {
        path: '/profile/article/:id',
        name:'articleShow',
        component: ArticleShow,
        beforeEnter: (to, from, next) => {
            onlyValid(next);
        }
    },
    {
      //Update password
      path:'/profile/change-password',
      name: 'changePassword',
      component: ChangePassword,
      beforeEnter: (to, from, next) => {
        onlyValid(next);
      }
    },
    {
      path:'/buyer',
      name:'soyComprador',
      component: SoyComprador,
      beforeEnter:(to, from, next)=>{
        onlyValid(next)
      }
    },
    {
      path:'/writter',
      name:'soyRedactor',
      component: SoyRedactor,
      beforeEnter:(to, from, next)=>{
        onlyValid(next)
      }
    },
    {
      path:'/writter/upload-article',
      name:'sendArticle',
      component: SendArticle,
      beforeEnter:(to, from, next)=>{
        onlyValid(next)
      }
    },
    {
        path: '/writter/test/:id',
        name: 'testStart',
        component: TestStart,
        beforeEnter:(to, from, next)=>{
          onlyValid(next)
        }
    }

  ]
})
